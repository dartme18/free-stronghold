DEBUG=true
ifeq "$(DEBUG)" "true"
DEBUGOPTS=-g -O0 -fsanitize=address -fsanitize=undefined -fno-sanitize-recover=all -fsanitize=float-divide-by-zero -fsanitize=float-cast-overflow -fno-sanitize=null -fno-sanitize=alignment
else
DEBUGOPTS=-O3 -DNDEBUG
endif
CC=g++
# To check and make sure stdafx is being used properly, add -H
CXXFLAGS=-Wall -Wextra -Werror -pedantic -std=c++17 $(DEBUGOPTS)
sources=$(wildcard src/*.cpp)
objects=$(sources:src/%.cpp=obj/%.o)
headers=$(wildcard src/*.hpp)
precompiledheader=src/stdafx.hpp.gch
executable=dist/freesh
resourcesrc=$(shell find resources/ -type f)
resourcedest=$(resourcesrc:resources/%=dist/%)
headerdependencies=$(objects:obj/%.o=obj/%.d)

# "|" signals an order-only prerequisite: the prereq must exist, but
# it won't cause the target to be recreated.
$(executable): $(precompiledheader) $(objects) Makefile | obj dist $(resourcedest)
	@echo Making $@
	@$(CC) $(CXXFLAGS) -o $@ $(objects) -lstdc++fs -lpthread -lGLEW -lglfw -lGL

$(resourcedest) : dist/% : resources/% | dist
	mkdir -p $(dir $@);
	cp -r $< $@;

obj dist:
	if [[ ! -e $@ ]]; then mkdir $@; fi;

-include $(headerdependencies)

# Static pattern rules. Take each object in the targets, create a "base"
# with it specified by % (removing the literal part '.o'), then
# use that base to generate the prerequisite. This generates
# several rules.  For instance,  main.o : "main".o : "main".cpp
# then refer to the left-most prerequisite with $<, and the target
# with $@ like usual.
$(objects) : obj/%.o : src/%.cpp $(precompiledheader) Makefile | obj
	@echo Making $@
	@$(CC) -c $(CXXFLAGS) -MMD -Isrc/spdlog/include $< -o $@

$(precompiledheader) : src/stdafx.hpp Makefile | obj
	@echo Making $@
	@$(CC) $(CXXFLAGS) -Isrc/spdlog/include $< -o $@

clean :
	rm -rf obj dist $(precompiledheader)

echo :
	@echo sources $(sources)
	@echo objects $(objects)
	@echo headers $(headers) src/stdafx.hpp
	@echo executable $(executable)
	@echo resourcesrc $(resourcesrc)
	@echo resourcedest $(resourcedest)

.PHONY : echo clean reformat
