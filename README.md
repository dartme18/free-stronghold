# Free Stronghold

This is Free Software (MIT-licensed), so feel free to learn from the source code, copy it, modify it, and share your modifications!

# Getting Assets

Assets exist inside the original Stronghold media.
The best documentation for the process of extracting the media is in the script, `processstrongholdfiles`.
Run it like this:

    ./processstrongholdfiles <datadir>;

where `datadir` is the directory where `data1.cab` (from the Stronghold Crusaders installation CD) can be found.
For instance, if you have mounted the disk (or an ISO) at `/mnt/stronghold` like so:

    sudo mkdir /mnt/stronghold;
    sudo mount /dev/cdrom /mnt/stronghold;

then you should call `./processstrongholdfiles /mnt/stronghold/Disk1;`.
The script will extract all files from the `.cab` file into a temp directory, move them to `./strongholdresources`, then
    delete the temporary directory.
I couldn't find a way to get `unshield` to extract only some files.

# Developers

## Building locally

### GNU/Linux (X11)

1. Clone
1. `git submodule update && git submodule init;` (The logging infrastructure, spdlog, is imported as a submodule.)
1. Install glew, glfw, and an opengl dispatch library (libglvnd)
1. `./mkrun`

# Troubleshooting

If you have any difficulty building or running this project, please [create a ticket](../../issues/new) so that we can work through it
and improve the experience for you and anyone like you!

