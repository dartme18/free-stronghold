#include "stdafx.hpp"

#include "camera.hpp"

// All units in meters
float const left_right_distance = .06;
float const default_move = .2;
float const default_look = 3;

camera::camera(glm::vec3 position, glm::vec3 look_at, glm::vec3 up, float field_of_view,
            float aspect_ratio, float near_plane, float far_plane) noexcept
    : dirty{true}
    , position{position}
    , look_at{look_at}
    , up{up}
    , field_of_view{field_of_view}
    , original_field_of_view{field_of_view}
    , aspect_ratio{aspect_ratio}
    , near_plane{near_plane}
    , far_plane{far_plane}
{
    set_matrix();
}

void camera::set_matrix(void) noexcept
{
    if (position == look_at)
    {
        WARN("Position and look_at are equivalent ({}). Moving look_at.", glm::to_string(position));
        look_at += glm::vec3{default_move, 0,0};
    }
    double const tangent = glm::tan(field_of_view/2);
    double const height = near_plane * tangent;
    double const width = height * aspect_ratio;
    // params: left, right, bottom, top, near, far
    auto const perspective = glm::frustum<float>(-width, width, -height, height, near_plane, far_plane);
    auto const mono_view = glm::lookAt(position, look_at, up);
    mat = perspective * mono_view;
    dirty = false;
}

glm::mat4 const & camera::get_matrix(void) noexcept
{
    if (dirty)
        set_matrix();
    return mat;
}

void camera::zoom_in(void) noexcept
{
    field_of_view -= field_of_view * .03;
    if (field_of_view < .02)
        field_of_view = .02;
    dirty = true;
}

void camera::zoom_out(void) noexcept
{
    field_of_view += field_of_view * .03;
    if (field_of_view > glm::radians(178.0f))
        field_of_view = glm::radians(178.0f);
    dirty = true;
}

void camera::reset_zoom(void) noexcept
{
    field_of_view = original_field_of_view;
    dirty = true;
}

void camera::move(glm::vec3 const & delta) noexcept
{
    auto look_at_vec {look_at - position};
    auto left {glm::cross(up, look_at_vec)};
    left = glm::normalize(left);
    auto forward = look_at_vec;
    forward.y = 0;
    forward = glm::normalize(forward);
    auto move { (left * -delta.x)+  (up * delta.y)+(   forward * -delta.z)};
    position += move;
    look_at += move;
    dirty = true;
}

void camera::move_back(void) noexcept
{
    move({0, 0, default_move});
}

void camera::move_forward(void) noexcept
{
    move({0, 0, -default_move});
}

void camera::strafe_left(void) noexcept
{
    move({-default_move, 0,0});
}

void camera::strafe_right(void) noexcept
{
    move({default_move,0,0});
}

void camera::move_up(void) noexcept
{
    move({0,default_move,0});
}

void camera::move_down(void) noexcept
{
    move({0,-default_move,0});
}

void camera::look_left(void) noexcept
{
    look({-default_look, 0});
}

void camera::look_right(void) noexcept
{
    look({default_look, 0});
}

void camera::look_up(void) noexcept
{
    look({0, default_look});
}

void camera::look_down(void) noexcept
{
    look({0, -default_look});
}

void camera::look(glm::vec2 const & delta) noexcept
{
    glm::vec3 const original_look_at_vec {look_at - position};
    glm::vec3 const left {glm::normalize(glm::cross(up, original_look_at_vec))};
    glm::vec3 const local_up {glm::normalize(glm::cross(original_look_at_vec, left))};
    glm::mat4 const horizontal_matr {glm::rotate(glm::mat4{1}, -delta.x * .001f, local_up)};
    glm::vec3 new_look_at_vec { rotate(horizontal_matr, original_look_at_vec)};
    glm::mat4 const vertical_matr {glm::rotate(glm::mat4{1}, -delta.y * .001f, left)};
    new_look_at_vec = rotate(vertical_matr, new_look_at_vec);
    // Don't look completely vertical
    // y is up.
    glm::vec3 const normalized{glm::normalize(new_look_at_vec)};
    float non_vertical {glm::length(glm::vec2{normalized.x, normalized.z})};
    if (std::abs(non_vertical) < .1)
        // Apply only the horizontal portion of this look operation
        new_look_at_vec = rotate(horizontal_matr, original_look_at_vec);
    look_at = new_look_at_vec + position;
    dirty = true;
}

glm::vec3 const & camera::get_position(void) const noexcept
{
    return position;
}

std::string camera::to_string(void) const noexcept
{
    std::ostringstream str;
    str << "Pos: " << position << " look: " << look_at << " up: " << up <<
        " fov: " << field_of_view << " ar: " << aspect_ratio << " near: " << near_plane <<
        " far: " << far_plane;
    return str.str();
}

