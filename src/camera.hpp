#pragma once
#include "utility.hpp"

struct camera
{
    camera(glm::vec3 position, glm::vec3 look_at, glm::vec3 up, float field_of_view,
            float aspect_ratio, float near_plane, float far_plane) noexcept;
    void strafe_right(void) noexcept;
    void strafe_left(void) noexcept;
    void move_back(void) noexcept;
    void move_forward(void) noexcept;
    void move_up(void) noexcept;
    void move_down(void) noexcept;
    void look_up(void) noexcept;
    void look_down(void) noexcept;
    void look_right(void) noexcept;
    void look_left(void) noexcept;
    void look(glm::vec2 const &) noexcept;
    void zoom_in(void) noexcept;
    void zoom_out(void) noexcept;
    void reset_zoom(void) noexcept;

    void move(glm::vec3 const &) noexcept;

    // The returned reference is only good until the position changes
    glm::vec3 const & get_position(void) const noexcept;

    std::string to_string(void) const noexcept;

    /**
     * Returns the combined "view" and "projection" matrices.
     * The reference is only good until a change is made to the
     * camera.
     */
    glm::mat4 const & get_matrix(void) noexcept;
private:
    void set_matrix(void) noexcept;
    bool dirty;
    glm::mat4 mat;
    glm::vec3 position;
    glm::vec3 look_at;
    glm::vec3 up;
    float field_of_view;
    float const original_field_of_view;
    float aspect_ratio;
    float near_plane;
    float far_plane;
};
