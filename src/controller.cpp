#include "stdafx.hpp"

#include "controller.hpp"
#include "tile_explorer.hpp"
#include "renderer.hpp"
#include "user_input.hpp"

controller::controller(user_input &i, unsigned window_width, unsigned window_height)
    : input{i}
    , cam{/*position*/{7,7,7},/*lookat*/{0,0,0},/*up*/{0,1,0}, glm::radians(90.0), window_width/(float)window_height, .01, 2000}
    , _game{_renderer, window_width, window_height}
    , width{window_width}
    , height{window_height}
{
    i.register_mouse_move([this](mouse_move_t const & m){handle_mouse_move(m);});
    i.register_mouse_button([this](mouse_button_t const & m){handle_mouse_button(m);});
    i.register_key_down([this](key_event_t const & e){handle_key_down(e);});
    i.register_mouse_scroll([this](scroll_event_t const & e){handle_mouse_scroll(e);});
    //explorer = new tile_explorer{_renderer};
    isometric.set_aspect_ratio(window_width/window_height);
}

void controller::handle_mouse_button(mouse_button_t const & event) noexcept
{
    INFO("Mouse event: button {}, down {}.", event.button, event.down);
}

void controller::handle_mouse_move(mouse_move_t const & event) noexcept
{
    if (explorer)
    {
        cam.look(event.delta);
        return;
    }
    _game.mouse_move(event.absolute);
    mouse_position = event.absolute;
}

void controller::update(void) noexcept
{
    if (explorer)
    {
        explorer->update();
        return;
    }
    _game.update();
    glm::vec2 move{0,0};
    if (mouse_position.x == 0)
        move.x = -.1;
    if (mouse_position.y == 0)
        move.y = -.1;
    if (mouse_position.x == width)
        move.x = .1;
    if (mouse_position.y == height)
        move.y = .1;
    isometric.move(move);
}

void controller::render() noexcept
{
    if (explorer)
    {
        _renderer.render(explorer->get_sprites(), cam.get_matrix());
        _renderer.render(explorer->get_render_objects(), cam.get_matrix());
        return;
    }
    _renderer.render(_game.get_sprites(), isometric.get_matrix());
    _renderer.render(_game.get_render_objects(), isometric.get_matrix());
    _renderer.render_ui(_game.get_ui_sprites());
}

void controller::handle_key_down(key_event_t const & e) noexcept
{
    if (e == GLFW_KEY_ESCAPE)
        input.toggle_capture_mouse();
}

void controller::handle_mouse_scroll(scroll_event_t const & e) noexcept
{
    if (e > 0)
        isometric.zoom_in();
    else if (e < 0)
        isometric.zoom_out();
}
