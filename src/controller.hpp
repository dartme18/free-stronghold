#pragma once

#include "game.hpp"
#include "renderer.hpp"
#include "camera.hpp"
#include "isometric_camera.hpp"
#include "user_input.hpp"

struct controller final
{
    controller(struct user_input &, unsigned window_width, unsigned window_height);
    void update(void) noexcept;
    void render(void) noexcept;
private:
    renderer _renderer;
    user_input & input;
    camera cam;
    isometric_camera isometric;
    struct tile_explorer * explorer {nullptr};
    game _game;
    glm::vec2 mouse_position{0,0};
    unsigned width, height;

    void handle_mouse_move(mouse_move_t const &) noexcept;
    void handle_mouse_button(mouse_button_t const &) noexcept;
    void handle_key_down(key_event_t const &) noexcept;
    void handle_mouse_scroll(scroll_event_t const &) noexcept;
};
