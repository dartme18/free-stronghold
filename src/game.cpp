#include "stdafx.hpp"

#include "game.hpp"
#include "utility.hpp"
#include "gm1_converter.hpp"
#include "renderer.hpp"

static tile_type get_random_tile_type(void)
{
    return static_cast<tile_type>(std::floor(get_random_number()*4));
}

game::game(renderer & _renderer, unsigned window_width, unsigned window_height)
    : scene{_renderer}
    , width{40}
    , height{40}
    , window_width{window_width}
    , window_height{window_height}
    , _renderer{_renderer}
{
    render_objects.push_back(get_axis_lines(20));
    _renderer.init(render_objects.back());
    tiles = std::vector<tile_type>(width*height);
    std::generate(tiles.begin(), tiles.end(), &get_random_tile_type);
    ui_sprites.push_back(sprite{_renderer.resources.get_cursor(), glm::mat4{1}});
    unsigned spot_num{0};
    glm::mat4 const scale{glm::scale(glm::mat4{1}, glm::vec3{(30.0f)/32, 1, 1})};
    for (unsigned x{0}; x < width; ++x)
    {
        for (unsigned y{0}; y < height; ++y)
        {
            // The game space is a diamond with the first tile at 0,0,0, the next tile at 1,0,-1
            glm::vec3 coordinates{static_cast<float>(x)+y, 0, static_cast<float>(y)-x};
            // The pixel data in each tile are inset from the outside of the tile (they are diamonds
            // of pixel data on quads), so we need to overlap the tiles to get them to fit together.
            coordinates.x *= .5;
            coordinates.z *= .5;
            glm::mat4 const spot{glm::translate(glm::mat4{1}, coordinates)};
            sprite temp_sprite{0, spot * scale};
            switch (spot_num % 21)
            {
                case 0: temp_sprite.texture = _renderer.resources.get_tile0(); break;
                case 1: temp_sprite.texture = _renderer.resources.get_tile1(); break;
                case 2: temp_sprite.texture = _renderer.resources.get_tile2(); break;
                case 3: temp_sprite.texture = _renderer.resources.get_tile3(); break;
                case 4: temp_sprite.texture = _renderer.resources.get_tile4(); break;
                case 5: temp_sprite.texture = _renderer.resources.get_tile5(); break;
                case 6: temp_sprite.texture = _renderer.resources.get_tile6(); break;
                case 7: temp_sprite.texture = _renderer.resources.get_tile7(); break;
                case 8: temp_sprite.texture = _renderer.resources.get_tile8(); break;
                case 9: temp_sprite.texture = _renderer.resources.get_tile9(); break;
                case 10: temp_sprite.texture = _renderer.resources.get_tile10(); break;
                case 11: temp_sprite.texture = _renderer.resources.get_tile11(); break;
                case 12: temp_sprite.texture = _renderer.resources.get_tile12(); break;
                case 13: temp_sprite.texture = _renderer.resources.get_tile13(); break;
                case 14: temp_sprite.texture = _renderer.resources.get_tile14(); break;
                case 15: temp_sprite.texture = _renderer.resources.get_tile15(); break;
                case 16: temp_sprite.texture = _renderer.resources.get_tile16(); break;
                case 17: temp_sprite.texture = _renderer.resources.get_tile17(); break;
                case 18: temp_sprite.texture = _renderer.resources.get_tile18(); break;
                case 19: temp_sprite.texture = _renderer.resources.get_tile19(); break;
                case 20: temp_sprite.texture = _renderer.resources.get_tile20(); break;
                default: INFO("oops"); break;
            }
            sprites.push_back(std::move(temp_sprite));
            ++spot_num;
        }
    }
}

void game::update(void) noexcept
{
}

void game::mouse_move(glm::vec2 const & absolute) noexcept
{
    // put mouse_position in the range ([-1,1],[-1,1]). (-1,1) is the upper left like a Cartesian plane.
    mouse_position = glm::vec2{absolute.x / (window_width/2)-1, -absolute.y / (window_height/2)+1};
    // The square that the sprite renderer uses to render sprites is on the X,Z plane.
    // We rotate (negatively) about the X axis to put the sprite in the X,Y plane.
    auto rotate = glm::rotate(glm::mat4{1}, PI/2, {1,0,0});
    // Move the image so that the hot spot is at (0,0)
    auto hot_spot_translation = glm::translate(glm::mat4{1}, {-.92,0.05,0});
    // Scale it down to size
    auto scale = glm::scale(glm::mat4{1}, {.08,.1,0});
    // Put the mouse icon at the correct point on the screen (clip space)
    auto position_translation = glm::translate(glm::mat4{1}, {mouse_position, 0});
    ui_sprites.back().position = position_translation*scale*hot_spot_translation*rotate;
}
