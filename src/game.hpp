#pragma once

#include "scene.hpp"

enum tile_type
{
    grass, dirt
};

struct game final : public scene
{
    game(struct renderer &, unsigned width_width, unsigned window_height);
    void update(void) noexcept override;
    void mouse_move(glm::vec2 const &) noexcept;

private:
    unsigned width, height, window_width, window_height;
    std::vector<tile_type> tiles;
    glm::vec2 mouse_position;
    struct renderer & _renderer;
};
