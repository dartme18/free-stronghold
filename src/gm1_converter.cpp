#include "stdafx.hpp"

#include "gm1_converter.hpp"

unsigned const header_size{88};
unsigned const palette_size{5120};

template <typename A>
static void read(std::istream & in, A & num)
{
    in.read(reinterpret_cast<char*>(&num), sizeof(A));
}

static uint32_t read_uint32(std::istream & in)
{
    uint32_t ret;
    read(in, ret);
    return ret;
}

static uint16_t read_uint16(std::istream & in)
{
    uint16_t ret;
    read(in, ret);
    return ret;
}

static uint8_t read_uint8(std::istream & in)
{
    uint8_t ret;
    read(in, ret);
    return ret;
}

palette::palette(std::istream & in)
{
    color_tables.reserve(10);
    for (unsigned i{0}; i < 10; ++i)
        color_tables.emplace_back(in);
}

color_table::color_table(std::istream & in)
{
    //multi-byte read?
    for (unsigned i{0}; i < std::tuple_size<decltype(data)>::value; ++i)
        read(in, data[i]);
}

gm1_file::gm1_file(std::istream & in)
{
    in.seekg(12, std::ios_base::cur);
    read(in, pic_count);
    in.seekg(4, std::ios_base::cur);
    read(in, data_type);
    in.seekg(24, std::ios_base::cur);
    read(in, width);
    read(in, height);
    in.seekg(24, std::ios_base::cur);
    read(in, data_size);
    in.seekg(4, std::ios_base::cur);
    if (pic_count > 2100)
        throw std::logic_error{"Pic count too high. This is probably a file format problem."};
}

tgx_image::tgx_image(std::istream & in)
{
    if (in.tellg() != 0)
        // We could adjust this, but no need to do so has been identified
        throw std::logic_error{"Must read tgx_image from the beginning of the file"};
    in.seekg(0, std::ios_base::end);
    unsigned size{static_cast<unsigned>(in.tellg())};
    in.seekg(0, std::ios_base::beg);
    read(in, width);
    read(in, height);
    if (!in)
    {
        if (width == 0 || height == 0)
            // o.O
            return;
        throw std::invalid_argument{"Unexpected end of file after reading dimensions."};
    }
    read_image(in, size);
}

void image_data::read_header(std::istream & in, tgx_image & img)
{
    // When in a gm1 file, this width and height is 16 bits. They're
    // declared as 32 bits because when read alone, a tgx image
    // width and height are 32 bits.
    img.width = read_uint16(in);
    // Tiles report 19 pixels high even though they're hand-coded to 16 rows :shrug:
    img.height = read_uint16(in);
    read(in, offset_x);
    read(in, offset_y);
    read(in, image_part);
    read(in, image_part_count);
    read(in, tile_offset_y);
    read(in, direction);
    read(in, horizontal_offset);
    read(in, building_width);
    read(in, animation_color_table);
}

// Converts a pixel between the gm1 16-bit format and a 32-bit format.
static uint32_t convert_pixel(uint16_t const raw) noexcept
{
    uint8_t r = ((raw >> 10) & 0b11111) << 3;
    uint8_t g = ((raw >> 5) & 0b11111) << 3;
    uint8_t b = (raw & 0b11111) << 3;
    uint8_t a = ((raw >> 15) & 1) ? 255 : 0;
    return r << 24 | g << 16 | b << 8 | a;
}

// size is the size of this image including header data.
// already_read indicates how many of "size" bytes have already been read.
void tgx_image::read_image(std::istream & in, unsigned size, unsigned already_read, palette const * p, uint8_t const /*color_table*/)
{
    auto const original_file_position{in.tellg()};
    // The number of pixels that have already been written
    unsigned x{0};
    while (in)
    {
        if (pixel_data.size() >= (width*height))
            break;
        if (in.tellg()-original_file_position >= size-already_read)
            break;
        uint8_t token {read_uint8(in)};
        uint8_t token_type{static_cast<uint8_t>(token >> 5)};
        uint8_t length {static_cast<uint8_t>((token & 31)+1)};
        switch (token_type)
        {
            case 0: // several pixels
                for (unsigned i{0}; i < length; ++i)
                {
                    uint16_t raw_color;
                    if (p)
                        // When I use p->color_tables.at(color_table), the colors aren't right
                        raw_color = p->color_tables.at(0).data[read_uint8(in)];
                    else
                        raw_color = read_uint16(in);
                    uint32_t const color{convert_pixel(raw_color)};
                    pixel_data.push_back(color);
                }
                x+=length;
                break;
            case 1: // several transparent pixels
                pixel_data.insert(pixel_data.end(), length, 0);
                x += length;
                break;
            case 2: // repeated pixels
                {
                    uint16_t raw_color;
                    if (p)
                        // When I use p->color_tables.at(color_table), the colors aren't right
                        raw_color =p->color_tables.at(0).data[read_uint8(in)];
                    else
                        raw_color = read_uint16(in);
                    uint32_t const color{convert_pixel(raw_color)};
                    pixel_data.insert(pixel_data.end(), length, color);
                    x += length;
                }
                break;
            case 4: // transparent to the end of the line
                // This is the only way to end a row of pixels
                pixel_data.insert(pixel_data.end(), width-x, 0);
                if (pixel_data.size() % width)
                    ERROR("uneven pixel data!?");
                x = 0;
                break;
            default:
                ERROR("Unknown token type {} (length {}). Ignoring", token_type, length);
                break;
        }
    }
    if (pixel_data.size() < width*height)
    {
        // Apparently not all of the pixels in the image are actually specified.
        // Images frequently end in a few rows of transparent pixels, but stop short
        // of the end of the image. We'll fill the images with further transparent pixels.
        pixel_data.insert(pixel_data.end(), width*height - pixel_data.size(), 0);
    }
    int bytes_read{static_cast<int>(in.tellg()-original_file_position)};
    if (bytes_read < static_cast<int>(size - already_read))
    {
        // Some images have padding bytes at the end
        unsigned const padding_bytes{size-already_read - bytes_read};
        in.seekg(padding_bytes, std::ios_base::cur);
        bytes_read+=padding_bytes;
    }
}

static void read_offsets_sizes_headers(gm1_file & file, std::istream & in)
{
    file.images.insert(file.images.end(), file.pic_count, image_with_metadata{});
    // Read offsets
    for (unsigned i{0}; i < file.pic_count; ++i)
        file.images.at(i).data.offset = read_uint32(in);
    if (!in)
        return ERROR("Premature end of file after starting {} images.", file.pic_count);
    for (unsigned i{0}; i < file.pic_count; ++i)
        file.images.at(i).data.size = read_uint32(in);
    if (!in)
        return ERROR("Premature end of file after reading sizes.");
    for (unsigned i{0}; i < file.pic_count; ++i)
        file.images.at(i).data.read_header(in, file.images.at(i).image);
    if (!in)
        return ERROR("Premature end of file after reading headers.");
}

constexpr std::array<unsigned, tgx_image::tile_height> tile_widths {2,6,10,14,18,22,26,30,30,26,22,18,14,10,6,2};

std::vector<uint32_t> tgx_image::get_tile_pixels(void) const
{
    if (tile_data.size() != 256)
        throw std::invalid_argument{"Incorrect number of tile data. Perhaps this is an attempt "
            "to get tile data from a non-tile GM1 file?"};
    std::vector<uint32_t> ret;
    ret.reserve(tile_width * tile_height);
    auto tile_data_iterator{tile_data.begin()};
    for (unsigned row{0}; row < tile_widths.size(); ++row)
    {
        unsigned const row_width{tile_widths[row]};
        // Transparent pixels leading up to explicit tile pixels
        ret.insert(ret.end(), (tile_width-row_width)/2, 0);
        auto const end_iterator{tile_data_iterator + row_width};
        std::copy(tile_data_iterator, end_iterator, std::back_inserter(ret));
        tile_data_iterator = end_iterator;
        // Finish the row with transparent pixels
        ret.insert(ret.end(), (tile_width-row_width)/2, 0);
    }
    return ret;
}

static void read_image(gm1_file & file, std::istream & in, image_with_metadata & img, palette & p)
{
    switch (file.data_type)
    {
        case gm1_file::DATA_TYPE_INTERFACE: // No palette
        case gm1_file::DATA_TYPE_FONT: // No palette
        case gm1_file::DATA_TYPE_CONSTANTSIZE: // No palette
            img.image.read_image(in, img.data.size);
            break;
        case gm1_file::DATA_TYPE_ANIMATION:
            img.image.read_image(in, img.data.size, 0, &p, img.data.animation_color_table);
            break;
        case gm1_file::DATA_TYPE_TILE:
            for (unsigned line{0}; line < tile_widths.size(); ++line)
                for (unsigned i{0}; i < tile_widths.at(line); ++i)
                    img.image.tile_data.push_back(convert_pixel(read_uint16(in)));
            // The tile is always 512 bytes long, so we need to tell the next function
            // that we've already 512 bytes of the image data.
            img.image.read_image(in, img.data.size, 512);
            break;
        case gm1_file::DATA_TYPE_UNCOMPRESSED1:
        case gm1_file::DATA_TYPE_UNCOMPRESSED2:
            for (unsigned x{0}; x < img.image.width; ++x)
                for (unsigned y{0}; y < img.image.height; ++y)
                    img.image.pixel_data.push_back(convert_pixel(read_uint16(in)));
            break;
        default:
            ERROR("Unknown gm1 file data type");
            break;
    }
}

static void read_images(gm1_file & file, std::istream & in, palette & p)
{
    for (unsigned i{0}; i < file.pic_count; ++i)
        read_image(file, in, file.images.at(i), p);
}

static void read_tiles(gm1_file & file, std::istream & in, palette & p)
{
    unsigned collection_count{0};
    for (unsigned i{0}; i < file.images.size(); ++i)
        if (!file.images.at(i).data.image_part)
            ++collection_count;
    std::vector<std::pair<uint32_t,uint32_t>> entries{collection_count};
    unsigned current_entry{0};
    for (unsigned n{0}; n < collection_count; ++n)
    {
        uint32_t top{std::numeric_limits<uint32_t>::max()};
        uint32_t left {top};
        uint32_t bottom{0};
        uint32_t right{0};
        uint32_t count{file.images.at(current_entry).data.image_part_count};
        for (unsigned i{current_entry}; i < current_entry + count; ++i)
        {
            left = std::min(left, static_cast<uint32_t>(file.images.at(i).data.offset_x));
            top = std::min(top, static_cast<uint32_t>(file.images.at(i).data.offset_y));
            right = std::max(right, static_cast<uint32_t>(file.images.at(i).data.offset_x + file.images.at(i).image.width));
            bottom = std::max(bottom, static_cast<uint32_t>(file.images.at(i).data.offset_y + file.images.at(i).image.height));
        }
        for (unsigned i{0}; i < count; ++i, ++current_entry)
        {
            image_with_metadata & img{file.images.at(current_entry)};
            img.data.collection = n;
            img.data.off_x = img.data.offset_x + img.data.horizontal_offset - left;
            img.data.off_y = img.data.offset_y - top;
            img.data.tile_x = img.data.offset_x - left;
            img.data.tile_y = img.data.offset_y + img.data.tile_offset_y-top;
        }
        entries.at(n) = {right-left, bottom-top};
    }
    read_images(file, in, p);
}

gm1_file read_gm1_file(std::string const & input)
{
    std::ifstream in{input};
    if (!in)
        throw std::invalid_argument{"Failed to read input file"};
    gm1_file file{in};
    if (!in)
        throw std::invalid_argument{"Premature end of file after header"};
    palette p{in};
    if (!in)
        throw std::invalid_argument{"Premature end of file after palette"};
    read_offsets_sizes_headers(file, in);
    if (!in)
        throw std::invalid_argument{"Premature end of file after offsets, sizes, and headers"};
    switch (file.data_type)
    {
        case gm1_file::DATA_TYPE_INTERFACE:
        case gm1_file::DATA_TYPE_ANIMATION:
        case gm1_file::DATA_TYPE_FONT:
        case gm1_file::DATA_TYPE_CONSTANTSIZE:
            read_images(file, in, p);
            break;
        case gm1_file::DATA_TYPE_TILE:
            read_tiles(file, in, p);
            break;
        default: throw std::logic_error(std::string{"Cannot read "} + std::to_string(file.data_type) + " images.");
    }
    // Apparently there is always one last byte at the end of the file?
    read_uint8(in);
    if (in)
        // We didn't read all bytes?
        WARN("Extra data in file buffer?");
    return file;
}

tgx_image read_tgx_file(std::string const & input)
{
    std::ifstream in{input};
    if (!in)
        throw std::invalid_argument{"Unable to read input file :\"" + input + "\""};
    return {in};
}
