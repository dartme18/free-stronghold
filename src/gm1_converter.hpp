#pragma once

struct color_table
{
    color_table(std::istream &);
    std::array<uint16_t, 256> data{};
};

struct palette
{
    palette(std::istream &);
    std::vector<color_table> color_tables;
};

struct tgx_image
{
    static const unsigned tile_width{30};
    static const unsigned tile_height{16};
    // This is used for stand-alone tgx files
    tgx_image(std::istream &);
    tgx_image(void) noexcept {}
    // Do not use width or height member fields for the tiles (see note on those fields).
    std::vector<uint32_t> get_tile_pixels(void) const;
    // Should not be called until read_header is called
    // Sometimes part of the image is already read before calling this function. In that case,
    // already_read should be set to a non-zero value.
    void read_image(std::istream &, unsigned size, unsigned already_read=0, palette const * = nullptr, uint8_t color_table = 0);
    // Width and height are 32 bits when the tgx file is on its own, but 16 bits
    // when part of a GM1 file.
    // Only for use on "pixel_data". For "tile_data", use tile_width and tile_height.
    uint32_t width;
    uint32_t height;
    std::vector<uint32_t> pixel_data;
    // Raw tile data. Use get_tile_pixels to format these data into a rectangular shape
    std::vector<uint32_t> tile_data;
};

struct image_data
{
    void read_header(std::istream &, tgx_image &);
    // Offset (in bytes) into the file of this picture
    uint32_t offset;
    // Size of the image in bytes
    uint32_t size;
    uint16_t offset_x;
    uint16_t offset_y;
    // Helps identify collections for tiles.
    uint8_t image_part;
    uint8_t image_part_count;
    uint16_t tile_offset_y;
    enum direction_t : uint8_t
    {
        DIRECTION_NONE = 0,
        DIRECTION_DOWN,
        DIRECTION_RIGHT,
        DIRECTION_LEFT,
    } direction;
    uint8_t horizontal_offset;
    uint8_t building_width;
    // I think this is used to select a color table for animations
    uint8_t animation_color_table;
    // Computed; not serialized
    unsigned collection;
    unsigned off_x;
    unsigned off_y;
    unsigned tile_x;
    unsigned tile_y;
};

struct image_with_metadata
{
    tgx_image image;
    image_data data;
};

struct gm1_file
{
    gm1_file(std::istream &);
    uint32_t pic_count;
    enum data_type_t: uint32_t
    {
        DATA_TYPE_INTERFACE = 1,
        DATA_TYPE_ANIMATION,
        DATA_TYPE_TILE,
        DATA_TYPE_FONT,
        DATA_TYPE_UNCOMPRESSED1,
        DATA_TYPE_CONSTANTSIZE,
        DATA_TYPE_UNCOMPRESSED2,
    } data_type;
    uint32_t width;
    uint32_t height;
    uint32_t data_size;
    std::vector<image_with_metadata> images;
};

gm1_file read_gm1_file(std::string const & input_file);
tgx_image read_tgx_file(std::string const & input_file);
