#include "stdafx.hpp"

#include "isometric_camera.hpp"
#include "utility.hpp"

// All units in meters
float const left_right_distance = .06;
float const default_move = .2;

void isometric_camera::set_matrix(void) noexcept
{
    if (position == look_at)
    {
        WARN("Position and look_at are equivalent ({}). Moving look_at.", glm::to_string(position));
        look_at += glm::vec3{default_move, 0,0};
    }
    float const view_width = view_height * aspect_ratio;
    glm::mat4 const perspective{glm::ortho(-view_width/2, view_width/2, -view_height/2,view_height/2, -10000000.0f,10000000.0f)};
    auto const view = glm::lookAt(position, look_at, up);
    mat = perspective * view;
    dirty = false;
}

glm::mat4 const & isometric_camera::get_matrix(void) noexcept
{
    if (dirty)
        set_matrix();
    return mat;
}

void isometric_camera::zoom_in(void) noexcept
{
    view_height -= view_height * .03;
    if (view_height < .02)
        view_height = .02;
    dirty = true;
}

void isometric_camera::zoom_out(void) noexcept
{
    view_height += view_height * .03;
    if (view_height > 200)
        view_height = 200;
    dirty = true;
}

void isometric_camera::reset_zoom(void) noexcept
{
    view_height = 30;
    dirty = true;
}

void isometric_camera::move(glm::vec2 const & delta) noexcept
{
    glm::vec3 look_at_vec {look_at - position};
    glm::vec3 left {glm::cross(up, look_at_vec)};
    left = glm::normalize(left);
    glm::vec3 forward = look_at_vec;
    forward.y = 0;
    forward = glm::normalize(forward);
    glm::vec3 const move { (left * -delta.x)+ (forward * -delta.y)};
    position += move;
    look_at += move;
    dirty = true;
}

void isometric_camera::move_back(void) noexcept
{
    move({0, default_move});
}

void isometric_camera::move_forward(void) noexcept
{
    move({0, -default_move});
}

void isometric_camera::move_left(void) noexcept
{
    move({default_move,0});
}

void isometric_camera::move_right(void) noexcept
{
    move({-default_move,0});
}

glm::vec3 const & isometric_camera::get_position(void) const noexcept
{
    return position;
}

std::string isometric_camera::to_string(void) const noexcept
{
    std::ostringstream str;
    str << "Pos: " << position << " look: " << look_at << " up: " << up <<
        " ar: " << aspect_ratio;
    return str.str();
}

void isometric_camera::set_aspect_ratio(double num) noexcept
{
    aspect_ratio = num;
}
