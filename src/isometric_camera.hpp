#pragma once

struct isometric_camera
{
    void move_back(void) noexcept;
    void move_forward(void) noexcept;
    void move_right(void) noexcept;
    void move_left(void) noexcept;
    void zoom_in(void) noexcept;
    void zoom_out(void) noexcept;
    void reset_zoom(void) noexcept;

    void move(glm::vec2 const &) noexcept;

    // The returned reference is only good until the position changes
    glm::vec3 const & get_position(void) const noexcept;

    std::string to_string(void) const noexcept;

    /**
     * Returns the combined "view" and "projection" matrices.
     * The reference is only good until a change is made to the
     * camera.
     */
    glm::mat4 const & get_matrix(void) noexcept;
    void set_aspect_ratio(double) noexcept;
private:
    void set_matrix(void) noexcept;
    bool dirty{true};
    glm::mat4 mat{1};
    glm::vec3 position{10,10,0};
    glm::vec3 look_at{0,0,0};
    glm::vec3 up{0,1,0};
    float view_height{10};
    float aspect_ratio{1};
};
