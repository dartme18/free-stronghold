#include "stdafx.hpp"
#include "objects.hpp"
#include "stopwatch.hpp"
#include "user_input.hpp"
#include "camera.hpp"
#include "controller.hpp"
#include "gm1_converter.hpp"
#include "resource_manager.hpp"

bool const full_screen {false};
unsigned const width {1000};
unsigned const height {800};

GLFWwindow * window;

void glfw_error_callback(int error, const char* description)
{
    ERROR("GLFW error {}: {}.", error, description);
}

bool init(void)
{
    glfwSetErrorCallback(&glfw_error_callback);
    glewExperimental = GL_TRUE;
    auto status {glfwInit()};
    if (status == GL_FALSE)
    {
        ERROR("glfwinit failed.");
        return false;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    if (full_screen)
        window = glfwCreateWindow(width, height, "Opengl", glfwGetPrimaryMonitor(), nullptr);
    else
        window = glfwCreateWindow(width, height, "OpenGL", nullptr, nullptr);
    if (!window)
    {
        ERROR("No window created");
        return false;
    }
    glfwMakeContextCurrent(window);
    // This has to be done after context creation.
    glewInit();
    // Depth testing disables transparency
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    //glFrontFace(GL_CW);
    //glCullFace(GL_BACK);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Set to 0 to disable vsync, 1 to enable vsync.
    glfwSwapInterval(1);
    //glXSwapIntervalMESA(0);
    return true;
}

int main(int /*argc*/, char const ** /*args*/)
{
    if (!init())
        std::exit(1);
    set_thread_name("main");
    DEBUG("Done initing");
    user_input input {window};
    controller cont{input, width, height};
    glClearColor(1,1,1,1);
    stopwatch total, draw, swap_buffers, poll;
    unsigned frames{0};
    total.start();
    while(!glfwWindowShouldClose(window))
    {
        draw.reset();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        cont.update();
        cont.render();
        draw.pause();
        swap_buffers.reset();
        glfwSwapBuffers(window);
        swap_buffers.pause();
        poll.reset();
        glfwPollEvents();
        poll.pause();
        if (++frames % 1000 == 0)
        {
            DEBUG("rendered {} frames in {} ms. {} fps.", frames, total.get_ms(), frames/(total.get_ms()/1000));
            DEBUG("  swap buffers {} draw {} poll {}", swap_buffers.get_ms(), draw.get_ms(), poll.get_ms());
            frames = 0;
            total.reset();
        }
    }
    glfwTerminate();
    return 0;
}
