#include "stdafx.hpp"

#include "objects.hpp"
#include "utility.hpp"
#include "render_object.hpp"

static void vec(std::vector<float> & v, float x, float y, float z) noexcept
{
    v.push_back(x); v.push_back(y); v.push_back(z);
}

static void vec(std::vector<float> & v, glm::vec3 const & push) noexcept
{
    vec(v, push.x, push.y, push.z);
}

template<typename ... As>
static void vec(std::vector<float> & push, glm::vec3 const & one_vec, As... vecs)
{
    vec(push, one_vec);
    vec(push, vecs...);
}

render_object get_axis_lines(float const size) noexcept
{
    std::vector<vertex> vertices;
    std::vector<GLuint> indices;
    vertices.push_back(from_position(size, 0, 0));
    vertices.push_back(from_position(0,0,0));
    vertices.push_back(from_position(0, size, 0));
    vertices.push_back(from_position(0, 0, size));
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(1);
    indices.push_back(3);
    indices.push_back(1);
    return render_object::line_list(std::move(vertices), std::move(indices));
}

std::vector<vertex> get_quad(glm::vec3 const & one, glm::vec3 const & two,
        glm::vec3 const & three, glm::vec3 const & four) noexcept
{
    std::vector<vertex> ret;
    ret.push_back({one, {0,0}});
    ret.push_back({two, {1,0}});
    // Creates one clockwise triangle and one counterclockwise triangle
    // Opengl treates the even triangles as opposite direction for culling purposes
    // which is just fine.
    ret.push_back({four, {0,1}});
    ret.push_back({three, {1,1}});
    return ret;
}

// Returns a square quad on the x,z plane with one corner or {0,0,0} the width and height specified
std::vector<vertex> get_square(float size) noexcept
{
    return get_quad({0,0,0}, {size,0,0},{size,0,size},{0,0,size});
}
