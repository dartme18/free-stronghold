#pragma once

#include "utility.hpp"

struct render_object get_axis_lines(float size) noexcept;

/**
 * Give the vertices in clockwise order.
 *
 * If the vertices (corners) are not co-planar, there will be a crease connecting
 * the second and fourth vertices.
 */
std::vector<vertex> get_quad(glm::vec3 const &, glm::vec3 const &, glm::vec3 const &, glm::vec3 const &) noexcept;

// Returns a square quad on the x,z plane with one corner or {0,0,0} the width and height specified
// This is a Triangle Strip as only four vertices are returned.
std::vector<vertex> get_square(float size = 1) noexcept;
