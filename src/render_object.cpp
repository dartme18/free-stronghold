#include "stdafx.hpp"

#include "render_object.hpp"

render_object render_object::triangles(std::vector<vertex> && v, std::vector<GLuint> && i) noexcept
{
    return render_object{std::move(v), std::move(i), GL_TRIANGLES};
}

render_object render_object::line_list(std::vector<vertex> && v, std::vector<GLuint> && i) noexcept
{
    return render_object{std::move(v), std::move(i), GL_LINES};
}

render_object::render_object(std::vector<vertex> && v, std::vector<GLuint> && i, GLenum const draw_mode) noexcept
    : position{1}, vertices{std::move(v)}, indices{std::move(i)}, draw_mode{draw_mode}
{
    glGenVertexArrays(1, &vao);
    glGenBuffers (1, &ebo);
    glGenBuffers (1, &vbo);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    if (indices.size())
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*indices.size(), indices.data(), GL_STATIC_DRAW);
    }
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex)*vertices.size(), vertices.data(), GL_STATIC_DRAW);
}

render_object::render_object(tgx_image const & img, bool tile, std::vector<vertex> && verts)
    : position{1}, vertices{std::move(verts)}, draw_mode{GL_TRIANGLE_STRIP}
{
    if (tile && !img.tile_data.size())
        throw std::invalid_argument{"No tile data associated with tgx_image."};
    glGenVertexArrays(1, &vao);
    glGenBuffers (1, &vbo);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex)*vertices.size(), vertices.data(), GL_STATIC_DRAW);
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
    if (tile)
        // Tiles are always 30x16 pixels
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 30, 16, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, img.get_tile_pixels().data());
    else
    // Instead of GL_RGB, consider GL_RGB5_A1 (... probably not)
    // Instead of GL_FLOAT, consider GL_UNSIGNED_SHORT_5_5_5_1
        glTexImage2D(GL_TEXTURE_2D, 0/* base image */, GL_RGB, img.width, img.height, 0/*reserved?*/, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, img.pixel_data.data());
}

render_object & render_object::operator=(render_object && other) noexcept
{
    std::swap(this->vertices, other.vertices);
    std::swap(this->indices, other.indices);
    std::swap(this->vao, other.vao);
    std::swap(this->dead, other.dead);
    std::swap(this->inited, other.inited);
    std::swap(this->position, other.position);
    std::swap(this->ebo, other.ebo);
    std::swap(this->vbo, other.vbo);
    std::swap(this->draw_mode, other.draw_mode);
    std::swap(this->tex, other.tex);
    return *this;
}

render_object::render_object(render_object && other) noexcept
    : position{other.position}
    , ebo{other.ebo}, vbo{other.vbo}, vao{other.vao}, tex{other.tex}
    , draw_mode{other.draw_mode}, dead{other.dead}, inited{other.inited}
{
    std::swap(this->vertices, other.vertices);
    std::swap(this->indices, other.indices);
    other.vao = GL_INVALID_VALUE;
    other.vbo = GL_INVALID_VALUE;
    other.ebo = GL_INVALID_VALUE;
    other.tex = GL_INVALID_VALUE;
    other.dead = true;
    other.inited = false;
}

void render_object::init(GLuint shaderProgram) noexcept
{
    if (inited)
        return;
    glBindVertexArray(vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    long unsigned const stride {sizeof(vertex)};
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, /*normalize*/GL_FALSE, stride, /*offset*/nullptr);
    glEnableVertexAttribArray(posAttrib);
    GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(vertex::position)));
    glEnableVertexAttribArray(texAttrib);
    inited = true;
}

render_object::~render_object() noexcept
{
    inited = false;
    if (dead)
        return;
    dead = true;
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &ebo);
    glDeleteBuffers(1, &vbo);
    if (tex != GL_INVALID_VALUE)
        glDeleteTextures(1, &tex);
}

void render_object::render(GLint const matrix_location) const noexcept
{
    if (!inited)
    {
        ERROR("Not drawing un-inited render_object: {} verts", vertices.size());
        return;
    }
    if (vertices.size())
    {
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glUniformMatrix4fv(matrix_location, 1, GL_FALSE, glm::value_ptr(position));
        if (tex != GL_INVALID_VALUE)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, tex);
        }
        if (indices.size())
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
            glDrawElements(draw_mode, indices.size(), GL_UNSIGNED_INT, /*index data?*/0);
        }
        else
            glDrawArrays(draw_mode, /*first index*/0, vertices.size());
    }
}

void render_object::set_position(glm::mat4 && mat) noexcept
{
    position = std::move(mat);
}
