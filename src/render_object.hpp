#pragma once
#include "gm1_converter.hpp"
#include "utility.hpp"
#include "objects.hpp"

/* A generic render object that holds its own vertices, indices, draw mode, and texture.
 * It's not a particularly optimal way to draw objects if you have a lot of them, but
 * it can meet some needs.
 */
struct render_object final
{
    render_object(std::vector<vertex> && vertices, std::vector<GLuint> && indices, GLenum draw_mode) noexcept;
    // If tile is true, this render object gets its pixel data from the "tile" part of the tgx_image.
    // If there is no tile portion of this image, throws a std::argument_exception.
    render_object(tgx_image const & img, bool tile, std::vector<vertex> && = get_square(1));
    static render_object triangles(std::vector<vertex> && vertices, std::vector<GLuint> && indices) noexcept;
    static render_object line_list(std::vector<vertex> && vertices, std::vector<GLuint> && indices) noexcept;

    render_object(render_object &&) noexcept;
    render_object & operator=(render_object &&) noexcept;
    // no copies
    render_object(render_object const &) = delete;
    // no copies
    render_object & operator=(render_object const &) = delete;
    // Not "virtual" because this struct is "final"
    ~render_object(void) noexcept;

    // Offers the render_object a chance to set vertex attributes
    void init(GLuint shaderProgram) noexcept;
    void render(GLint const matrix_loc) const noexcept;
    void set_position(glm::mat4 &&) noexcept;

private:
    glm::mat4 position;
    std::vector<vertex> vertices;
    std::vector<GLuint> indices;
    GLuint ebo{GL_INVALID_VALUE};
    GLuint vbo{GL_INVALID_VALUE};
    GLuint vao{GL_INVALID_VALUE};
    GLuint tex{GL_INVALID_VALUE};
    GLenum draw_mode{GL_TRIANGLES};
    bool dead{false};
    bool inited{false};
};
