#include "stdafx.hpp"

#include "sprite.hpp"
#include "renderer.hpp"
#include "render_object.hpp"

unsigned const seconds_per_day {200};
glm::vec3 const lightambient {.4,.4,.4};
glm::vec3 const lightdiffuse {.2,.2,.2};
glm::vec3 const lightspecular {.1,.1,.1};

#define GLSL(src) "#version 150 core\n" #src

char const * vertex_shader_source {GLSL(
    in vec3 position;
    in vec2 texcoord;
    out vec2 frag_tex_coord;
    uniform mat4 object;
    uniform mat4 view;
    void main()
    {
        gl_Position = view *  object * vec4(position, 1.0f);
        frag_tex_coord = texcoord;
    }
)};

char const * fragment_shader_source {GLSL(
    in vec2 frag_tex_coord;
    out vec4 output_color;
    uniform sampler2D tex;
    void main()
    {
        output_color = texture(tex, frag_tex_coord);
        //output_color.a = 1;
    }
)};

renderer::renderer() noexcept
    : start{std::chrono::high_resolution_clock::now()}
    , shader_program {glCreateProgram()}
{
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, nullptr);
    glShaderSource(fragment_shader, 1, &fragment_shader_source, nullptr);
    char buffer[512];
    GLint status;
    glCompileShader(vertex_shader);
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        glGetShaderInfoLog(vertex_shader, 512, nullptr, buffer);
        ERROR("Problem compiling vertex shader: {}", buffer);
    }
    glCompileShader(fragment_shader);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        glGetShaderInfoLog(fragment_shader, 512, nullptr, buffer);
        ERROR("Problem compiling fragment shader: {}", buffer);
    }
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);
    glGetProgramiv(shader_program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        glGetProgramInfoLog(shader_program, 512, nullptr, buffer);
        ERROR("Problem linking shader program: {}", buffer);
    }
    matrix_view_loc = glGetUniformLocation(shader_program, "view");
    matrix_object_loc = glGetUniformLocation(shader_program, "object");
    _sprite_renderer.initialize(shader_program);
}

renderer::~renderer(void) noexcept
{
    glDetachShader(shader_program, vertex_shader);
    glDetachShader(shader_program, fragment_shader);
    glDeleteProgram(shader_program);
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
}

void renderer::render(std::vector<render_object> const & objects, glm::mat4 const & mat) noexcept
{
    glUseProgram(shader_program);
    glUniformMatrix4fv(matrix_view_loc, 1, GL_FALSE, glm::value_ptr(mat));
    for (render_object const & o : objects)
        o.render(matrix_object_loc);
}

void renderer::render(std::vector<sprite> const & objects, glm::mat4 const & mat) noexcept
{
    glUseProgram(shader_program);
    glUniformMatrix4fv(matrix_view_loc, 1, GL_FALSE, glm::value_ptr(mat));
    for (sprite const & o : objects)
        _sprite_renderer.render(o);
}

void renderer::render_ui(std::vector<sprite> const & objects) noexcept
{
    static glm::mat4 identity{1};
    glUseProgram(shader_program);
    glUniformMatrix4fv(matrix_view_loc, 1, GL_FALSE, glm::value_ptr(identity));
    for (sprite const & o : objects)
        _sprite_renderer.render(o);
}

void renderer::init(render_object & r) noexcept
{
    r.init(shader_program);
}
