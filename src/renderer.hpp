#pragma once
#include "camera.hpp"
#include "sprite_renderer.hpp"
#include "resource_manager.hpp"

struct renderer
{
    renderer(void) noexcept;
    ~renderer(void) noexcept;

    void render(std::vector<struct render_object> const &, glm::mat4 const &) noexcept;
    void render(std::vector<sprite> const &, glm::mat4 const &) noexcept;
    void render_ui(std::vector<sprite> const &) noexcept;
    void init(struct render_object &) noexcept;
    resource_manager resources;
private:
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    GLuint vertex_shader{GL_INVALID_VALUE};
    GLuint fragment_shader{GL_INVALID_VALUE};
    GLuint shader_program{GL_INVALID_VALUE};
    GLuint ui_vertex_shader{GL_INVALID_VALUE};
    GLuint ui_fragment_shader{GL_INVALID_VALUE};
    GLuint ui_shader_program{GL_INVALID_VALUE};
    GLint matrix_view_loc{GL_INVALID_VALUE};
    GLint matrix_object_loc{GL_INVALID_VALUE};
    sprite_renderer _sprite_renderer;
};

