#include "stdafx.hpp"

#include "resource_manager.hpp"
#include "gm1_converter.hpp"

GLuint resource_manager::generate_texture(bool managed)
{
    GLuint texture;
    glGenTextures(1, &texture);
    if (texture == 0)
        ERROR("Failed to generate texture?");
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
    if (managed)
        textures.push_back(texture);
    return texture;
}

void resource_manager::load_texture_data(std::vector<uint32_t> const & pixels, GLuint texture, GLsizei width, GLsizei height)
{
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, pixels.data());
}

GLuint resource_manager::get_cursor(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/icons_front_end.gm1")};
        tgx_image const & img {gm1.images.at(0).image};
        load_texture_data(img.pixel_data, texture, img.width, img.height);
    }
    return texture;
}

GLuint resource_manager::get_grass_tile_texture(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(1).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile0(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(2).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile1(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(3).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile2(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(4).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile3(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(5).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile4(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(6).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile5(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(7).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile6(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(8).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile7(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(9).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile8(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(10).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile9(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(11).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile10(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(12).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile11(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(13).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile12(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(14).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile13(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(15).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile14(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(16).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile15(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(17).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile16(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(18).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile17(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(19).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile18(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(20).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile19(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(21).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

GLuint resource_manager::get_tile20(void)
{
    static GLuint texture{0};
    if (!texture)
    {
        texture = generate_texture();
        gm1_file const gm1{read_gm1_file("../strongholdresources/tile_land_macros.gm1")};
        tgx_image const & img {gm1.images.at(22).image};
        load_texture_data(img.get_tile_pixels(), texture, tgx_image::tile_width, tgx_image::tile_height);
    }
    return texture;
}

resource_manager::~resource_manager(void) noexcept
{
    for (GLuint tex : textures)
        glDeleteTextures(1, &tex);
}
