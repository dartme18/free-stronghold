#pragma once

struct resource_manager final
{
    GLuint get_grass_tile_texture(void);
    GLuint get_tile0(void);
    GLuint get_tile1(void);
    GLuint get_tile2(void);
    GLuint get_tile3(void);
    GLuint get_tile4(void);
    GLuint get_tile5(void);
    GLuint get_tile6(void);
    GLuint get_tile7(void);
    GLuint get_tile8(void);
    GLuint get_tile9(void);
    GLuint get_tile10(void);
    GLuint get_tile11(void);
    GLuint get_tile12(void);
    GLuint get_tile13(void);
    GLuint get_tile14(void);
    GLuint get_tile15(void);
    GLuint get_tile16(void);
    GLuint get_tile17(void);
    GLuint get_tile18(void);
    GLuint get_tile19(void);
    GLuint get_tile20(void);
    GLuint get_cursor(void);
    ~resource_manager(void) noexcept;
    // Creates and returns a texture whose lifetime is optionally tied to this object's lifetime
    // If managed is false, be sure to call glDeleteTextures when you're done with the texture.
    GLuint generate_texture(bool managed = true);
    // Load rgba(?) pixels into texture
    void load_texture_data(std::vector<uint32_t> const & pixels, GLuint texture, GLsizei width, GLsizei height);
private:
    std::vector<GLuint> textures;
};
