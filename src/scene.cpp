#include "stdafx.hpp"

#include "scene.hpp"
#include "renderer.hpp"

scene::scene(renderer & rend)
    : _renderer{rend}
{
}

std::vector<render_object> const & scene::get_render_objects(void) noexcept
{
    return render_objects;
}

std::vector<sprite> const & scene::get_sprites(void) noexcept
{
    return sprites;
}

std::vector<sprite> const & scene::get_ui_sprites(void) noexcept
{
    return ui_sprites;
}
