#pragma once

#include "sprite.hpp"
#include "render_object.hpp"

struct scene
{
    scene(struct renderer &);
    virtual ~scene(void){};
    virtual std::vector<sprite> const & get_sprites(void) noexcept;
    virtual std::vector<render_object> const & get_render_objects(void) noexcept;
    // These are rendered in "clip space" rather than "world" space.
    virtual std::vector<sprite> const & get_ui_sprites(void) noexcept;
    virtual void update(void) noexcept{};
protected:
    renderer & _renderer;
    std::vector<sprite> sprites;
    std::vector<render_object> render_objects;
    std::vector<sprite> ui_sprites;
};
