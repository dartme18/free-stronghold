#pragma once

struct sprite
{
    GLuint texture{GL_INVALID_VALUE};
    // The sprite is rendered by the sprite_renderer with corners at
    // {0,0,0} and {1,0,1}. This matrix moves the sprite wherever you want it.
    glm::mat4 position{1};
};
