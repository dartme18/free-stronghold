#include "stdafx.hpp"

#include "sprite_renderer.hpp"
#include "sprite.hpp"
#include "utility.hpp"
#include "objects.hpp"

void sprite_renderer::initialize(GLuint shader_program) noexcept
{
    glGenVertexArrays(1, &vao);
    glGenBuffers (1, &vbo);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    std::vector<vertex> vertices{get_square()};
    long unsigned const stride {sizeof(vertex)};
    glBufferData(GL_ARRAY_BUFFER, stride*vertices.size(), vertices.data(), GL_STATIC_DRAW);
    GLint posAttrib = glGetAttribLocation(shader_program, "position");
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, /*normalize*/GL_FALSE, stride, /*offset*/nullptr);
    glEnableVertexAttribArray(posAttrib);
    GLint texAttrib = glGetAttribLocation(shader_program, "texcoord");
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(vertex::position)));
    glEnableVertexAttribArray(texAttrib);
    matrix_loc = glGetUniformLocation(shader_program, "object");
    initialized = true;
}

sprite_renderer::~sprite_renderer() noexcept
{
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
}

void sprite_renderer::render(sprite const & s) const noexcept
{
    if (!initialized)
        return ERROR("Attempt to render sprite when not initialized");
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glUniformMatrix4fv(matrix_loc, 1, GL_FALSE, glm::value_ptr(s.position));
    if (s.texture != GL_INVALID_VALUE)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, s.texture);
    }
    glDrawArrays(GL_TRIANGLE_STRIP, /*first index*/0, /*number of vertices*/4);
}
