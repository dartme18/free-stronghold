#pragma once

struct sprite_renderer final
{
    ~sprite_renderer(void) noexcept;
    void initialize(GLuint shader_program) noexcept;
    // Renders each sprite onto a quad with corners at {0,0,0} and {1,0,1}
    // Of course, the sprite also has a transformation matrix that will be applied, too.
    void render(struct sprite const &) const noexcept;
private:
    GLuint vbo{GL_INVALID_VALUE};
    GLuint vao{GL_INVALID_VALUE};
    GLint matrix_loc{GL_INVALID_VALUE};
    bool initialized{false};
};

