#include "stdafx.hpp"
#include "stopwatch.hpp"
#include "logging.hpp"

void stopwatch::start(void)
{
    if (!is_paused)
        return;
    saved_time = std::chrono::high_resolution_clock::now();
    is_paused = false;
}

void stopwatch::pause(void)
{
    if (is_paused)
        WARN("Attempt to pause a paused clock continuing.");
    accumulated += std::chrono::high_resolution_clock::now() - saved_time;
    is_paused = true;
}

void stopwatch::reset(void)
{
    is_paused = false;
    saved_time = {std::chrono::high_resolution_clock::now()};
    accumulated = {};
}

double stopwatch::get_ms(void) const
{
    auto ret {accumulated};
    auto add {std::chrono::high_resolution_clock::now() - saved_time};
    ret += add;
    return std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(ret).count();
}
