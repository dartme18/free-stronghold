#ifndef STOPWATCH_HEADER
#define STOPWATCH_HEADER
#include "stdafx.hpp"

struct stopwatch
{
    void start(void);
    void pause(void);
    // Clears all accumulated time and starts the stopwatch
    void reset(void);
    double get_ms(void) const;

private:
    std::chrono::time_point<std::chrono::high_resolution_clock> saved_time {std::chrono::high_resolution_clock::now()};
    std::chrono::high_resolution_clock::duration accumulated {};
    bool is_paused {true};
};

#endif
