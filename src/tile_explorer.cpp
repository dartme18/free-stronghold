#include "stdafx.hpp"

#include "tile_explorer.hpp"
#include "camera.hpp"
#include "user_input.hpp"
#include "utility.hpp"
#include "objects.hpp"
#include "renderer.hpp"

tile_explorer::tile_explorer(renderer & _renderer)
    : scene{_renderer}
{
    render_objects.push_back(get_axis_lines(20));
    _renderer.init(render_objects.back());
    float x{0};
    float const gap{.1};
    int const width{8};
    gm1file = read_gm1_file("../strongholdresources/body_peasant.gm1");
    std::vector<std::string> gm1_files {"cursors.gm1",// "body_cow.gm1", "body_peasant.gm1", "anim_armourer.gm1",
        // Too many textures?
        //"tile_data.gm1", "tile_rocks8.gm1", "tile_land3.gm1", "tile_land8.gm1", "super chicken.gm1",
        //"puff of smoke.gm1", "font_slanted.gm1", "font_stronghold_aa.gm1", "font_stronghold.gm1",
        //"float_pop_circ.gm1", "floats.gm1", "floats_new.gm1", "cracks.gm1", "face800-blank.gm1",
        "icons_front_end.gm1", "icons_front_end_builder.gm1", "icons_front_end_combat.gm1",
        "icons_placeholders.gm1", "interface_buttons.gm1", "interface_ruins.gm1",
        "mini_cursors.gm1"
   //     "tile_land_macros.gm1",
    };
    for (unsigned f{0}; f < gm1_files.size(); ++f)
    {
        INFO("Loading {}...", gm1_files[f]);
        try
        {
            gm1_file const gm1{read_gm1_file("../strongholdresources/" + gm1_files[f])};
            for (unsigned i {0}; i < gm1.images.size(); ++i)
            {
                GLuint tex{_renderer.resources.generate_texture()};
                tgx_image const & image {gm1.images[i].image};
                _renderer.resources.load_texture_data(image.pixel_data, tex, image.width, image.height);
                glm::vec2 vec2_coords{get_coordinates(width, i)};
                glm::vec3 coordinates{vec2_coords.x, 0, vec2_coords.y};
                coordinates.x = coordinates.x*(tile_size+gap/3) + x;
                float additional{gap*std::floor(coordinates.z/4)};
                coordinates.z = coordinates.z*(tile_size+gap/3) + additional;
                glm::mat4 translate {glm::translate(glm::mat4{1}, {coordinates.x, 0, coordinates.z})};
                glm::mat4 scale{glm::scale(glm::mat4{1}, {tile_size, 1, tile_size})};
                sprites.push_back(sprite{tex, translate * scale});
            }
            x += (tile_size+gap) * width + tile_size+gap;
            if (gm1.data_type == gm1_file::DATA_TYPE_TILE)
            {
                for (unsigned i{0}; i < gm1.images.size(); ++i)
                {
                    GLuint tex{_renderer.resources.generate_texture()};
                    tgx_image const & image {gm1.images[i].image};
                    _renderer.resources.load_texture_data(image.get_tile_pixels(), tex, tgx_image::tile_width, tgx_image::tile_height);
                    glm::vec2 const coordinates{get_coordinates(width, i)};
                    glm::mat4 translate {glm::translate(glm::mat4{1}, {coordinates.x * 2*(tile_size+gap) + x, 0, coordinates.y*(tile_size+gap)})};
                    glm::mat4 scale {glm::scale(glm::mat4{1}, {tile_size*2,1,tile_size})};
                    sprites.push_back(sprite{tex, translate * scale});
                }
                x += (tile_size+gap) * width *2 + tile_size+gap;
            }
        }
        catch (std::logic_error const &e)
        {
            INFO("Skipping {} due to error: {}", gm1_files[f], e.what());
        }
    }
    x = 0;
    std::vector<std::string> tgx_files {
        "bullet.tgx", "churchs.tgx", "church_sketch.tgx"
    };
    for (std::string const & tgx_file:tgx_files)
    {
        GLuint tex{_renderer.resources.generate_texture()};
        tgx_image img {read_tgx_file("../strongholdresources/" + tgx_file)};
        _renderer.resources.load_texture_data(img.pixel_data, tex, img.width, img.height);
        glm::mat4 translate {glm::translate(glm::mat4{1}, {x, 0, -1- gap})};
        sprites.push_back(sprite{tex, std::move(translate)});
        x += 1 + gap;
    }
    sprites.push_back({static_cast<GLuint>(-1), {}});
    DEBUG("Tile explorer has {} objects.", sprites.size());
}

unsigned frame_count{0};
void tile_explorer::update(void) noexcept
{
    if (frame_count)
    {
        --frame_count;
        return;
    }
    frame_count = 14;
    // 16 walking frames in each direction (16*8 frames)
    // 12 squatting frames in each diagonal direction (12*4 frames)
    // 16 why-me frames in each diagonal direction (16*4 frames)
    // 16 thinking frames in each diagonal direction (16*4 frames)
    // 10 opening door frames in each diagonal direction (10*4 frames)
    // 12 bowing frames in each diagonal direction (12*4 frames)
    // 12 frames*4+1 frames per orientation, then death animations:
    //   death animation 1: 3*8 frames (shot by an arrow in the neck?)
    //   death animation 2: 3*8 frames (cut by a sword in the belly?)
    //   death animation 3: 3*8 frames (cut by a sword in the neck?)
    unsigned const walking_frames {(16)*8};
    unsigned const secondary_frames {(12+16+16+10+12)*4};
    if (object_index >= walking_frames)
    {
        if (object_index >= walking_frames + secondary_frames)
        {
            // Death animations
            ++object_index;
            if (object_index - walking_frames -secondary_frames > 3*8*3)
            {
                //Finished with death animations
                object_index = 0;
                frame_count /= 2;
            }
        }
        else
        {
            // secondary animations
            // normal animations
            object_index += 4;
            if (object_index >= walking_frames + secondary_frames)
            {
                DEBUG("Finished with {} secondary animation sequence.", object_index % 4);
                if (object_index % 4 == 3)
                {
                    DEBUG("Beginning death sequences");
                    object_index = walking_frames + secondary_frames;
                }
                else
                    // next secondary animation sequence
                    object_index = walking_frames + object_index % 4 + 1;
            }
        }
    }
    else
    {
        // normal animations
        object_index += 8;
        if (object_index >= walking_frames)
        {
            DEBUG("Finished with {} walking animation sequence.", object_index % 8);
            if (object_index % 8 == 7)
            {
                DEBUG("Beginning secondary sequences");
                object_index = walking_frames;
            }
            else
            {
                frame_count /= 2;
                object_index = object_index % 8 + 1;
            }
        }
        else
            frame_count /= 2;
    }
    if (object_index >= gm1file->images.size())
    {
        DEBUG("Death sequences done, starting over");
        object_index = 0;
    }
    GLuint tex{_renderer.resources.generate_texture(false)};
    tgx_image const & image {gm1file->images.at(object_index).image};
    _renderer.resources.load_texture_data(image.pixel_data, tex, image.width, image.height);
    glDeleteTextures(1, &sprites.back().texture);
    sprites.pop_back();
    sprites.push_back(sprite{tex, glm::translate(glm::scale(glm::mat4{1}, {15,1,15}), {0,10,0})});
}
