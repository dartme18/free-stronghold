#pragma once

#include "scene.hpp"
#include "gm1_converter.hpp"

struct tile_explorer final : public scene
{
    tile_explorer(struct renderer &);
    void update(void) noexcept override;

private:
    std::optional<gm1_file> gm1file{};
    unsigned object_index{0};
};
