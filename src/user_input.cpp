#include "stdafx.hpp"

#include "user_input.hpp"

// The callbacks cannot be instance member function, so this is how I'm connecting
// instances (and instance member functions) to the callbacks.
static std::map<GLFWwindow *, user_input *> user_inputs;

void user_input::mouse_position_callback(GLFWwindow* window, double xpos, double ypos) noexcept
{
    if (user_inputs.find(window) == user_inputs.end())
        return;
    user_inputs[window]->mouse_position(window, xpos, ypos);
}

void user_input::mouse_button_callback(GLFWwindow* window, int btn, int action, int mods) noexcept
{
    user_inputs[window]->mouse_button(window, btn, action, mods);
}

void user_input::wheel_scroll_callback(GLFWwindow * window, double x, double y) noexcept
{
    user_inputs[window]->mouse_scroll(window, x, y);
}

void user_input::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) noexcept
{
    user_inputs[window]->key(window, key, scancode, action, mods);
}

user_input::user_input(GLFWwindow * w)
    : window{w}
{
    if (user_inputs.find(window) != user_inputs.end())
        throw std::invalid_argument{"There is already a user_input handling that window."};
    user_inputs.insert({window, this});
    glfwSetMouseButtonCallback(window, &mouse_button_callback);
    glfwSetKeyCallback(window, &key_callback);
    glfwSetScrollCallback(window, &wheel_scroll_callback);
    glfwSetCursorPosCallback(window, &mouse_position_callback);
    glfwSetInputMode(window, GLFW_CURSOR, mouse_captured ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
    glfwSetCursorPos(window, mousepos.x,mousepos.y);
}

void user_input::key(GLFWwindow * window, int key, int /*scancode*/, int action, int /*mods*/) noexcept
{
    bool press = action == GLFW_PRESS || action == GLFW_REPEAT;
    if (press && key == GLFW_KEY_Q)
        glfwSetWindowShouldClose(window, GL_TRUE);
    if (press)
        for (auto const & handler : key_down_handlers)
            handler(key);
    else
        for (auto const & handler : key_up_handlers)
            handler(key);
}

void user_input::mouse_scroll(GLFWwindow *, double /*x*/, double y) noexcept
{
    for (auto const & handler : mouse_scroll_handlers)
        handler(y);
}

void user_input::mouse_button(GLFWwindow * /*window*/, int btn, int action, int /*mods*/) noexcept
{
    if (action == 1 && !mouse_captured)
    {
        set_capture_mouse(true);
        return;
    }
    for (auto const & handler : mouse_button_handlers)
        handler({btn, action==1});
}

void user_input::toggle_capture_mouse(void) noexcept
{
    set_capture_mouse(!mouse_captured);
}

void user_input::set_capture_mouse(bool cap) noexcept
{
    mouse_captured = cap;
    if (mouse_captured)
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    else
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void user_input::mouse_position(GLFWwindow *, double xpos, double ypos) noexcept
{
    glm::vec2 old_mouse_pos{mousepos};
    mousepos = {xpos, ypos};
    if (clamp_mouse)
        do_clamp_mouse();
    glm::vec2 delta = mousepos - old_mouse_pos;
    mouse_move_t event{mousepos, delta};
    for (auto const & handler: mouse_move_handlers)
        handler(event);
}

void user_input::do_clamp_mouse(void)
{
    int window_width, window_height;
    glfwGetWindowSize(window, &window_width, &window_height);
    bool changed{false};
    if (mousepos.x < 0)
    {
        changed = true;
        mousepos.x = 0;
    }
    if (mousepos.x > window_width)
    {
        changed = true;
        mousepos.x = window_width;
    }
    if (mousepos.y < 0)
    {
        changed = true;
        mousepos.y = 0;
    }
    if (mousepos.y > window_height)
    {
        changed = true;
        mousepos.y = window_height;
    }
    if (changed)
        glfwSetCursorPos(window, mousepos.x, mousepos.y);
}

void user_input::set_clamp_mouse_to_window(bool clamp) noexcept
{
    if (clamp == clamp_mouse)
        return;
    clamp_mouse = clamp;
    if (clamp_mouse)
        do_clamp_mouse();
}

void user_input::register_mouse_move(std::function<void(mouse_move_t const &)> const & handler) noexcept
{
    mouse_move_handlers.push_back(handler);
}

void user_input::register_mouse_button(std::function<void(mouse_button_t const &)> const & handler) noexcept
{
    mouse_button_handlers.push_back(handler);
}

void user_input::register_mouse_scroll(std::function<void(scroll_event_t const &)> const & handler) noexcept
{
    mouse_scroll_handlers.push_back(handler);
}

void user_input::register_key_up(std::function<void(key_event_t const &)> const & handler) noexcept
{
    key_up_handlers.push_back(handler);
}

void user_input::register_key_down(std::function<void(key_event_t const &)> const & handler) noexcept
{
    key_down_handlers.push_back(handler);
}
