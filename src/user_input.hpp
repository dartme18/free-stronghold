#pragma once

#include "utility.hpp"
#include "camera.hpp"

struct mouse_move_t
{
    glm::vec2 absolute;
    glm::vec2 delta;
};

struct mouse_button_t
{
    int button;
    bool down;
};

using key_event_t = int;
using scroll_event_t = double;

struct user_input
{
    user_input(GLFWwindow *);

    void register_mouse_move(std::function<void(mouse_move_t const &)> const &) noexcept;
    void register_mouse_button(std::function<void(mouse_button_t const &)> const &) noexcept;
    void register_mouse_scroll(std::function<void(scroll_event_t const &)> const &) noexcept;
    // Can be sent repeatedly for a key because the callback is called when the key first goes
    // down and any time GLFW issues a "repeat" event.
    void register_key_down(std::function<void(key_event_t const &)> const &) noexcept;
    void register_key_up(std::function<void(key_event_t const &)> const &) noexcept;
    // If true, the OS mouse is not shown
    void set_capture_mouse(bool) noexcept;
    void toggle_capture_mouse(void) noexcept;
    void set_clamp_mouse_to_window(bool) noexcept;

private:
    glm::vec2 mousepos{0,0};
    std::vector<std::function<void(mouse_move_t)>> mouse_move_handlers;
    std::vector<std::function<void(mouse_button_t)>> mouse_button_handlers;
    std::vector<std::function<void(scroll_event_t)>> mouse_scroll_handlers;
    std::vector<std::function<void(key_event_t)>> key_down_handlers;
    std::vector<std::function<void(key_event_t)>> key_up_handlers;
    bool mouse_captured{true};
    bool clamp_mouse{true};
    GLFWwindow * window{nullptr};

    static void mouse_position_callback(GLFWwindow* window, double xpos, double ypos) noexcept;
    static void mouse_button_callback(GLFWwindow* window, int btn, int action, int mods) noexcept;
    static void wheel_scroll_callback(GLFWwindow * window, double x, double y) noexcept;
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) noexcept;
    void mouse_position(GLFWwindow*, double xpos, double ypos) noexcept;
    void mouse_button(GLFWwindow*, int btn, int action, int mods) noexcept;
    void mouse_scroll(GLFWwindow *, double x, double y) noexcept;
    void key(GLFWwindow*, int key, int scancode, int action, int mods) noexcept;
    void do_clamp_mouse(void);
};

