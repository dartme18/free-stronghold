#include "stdafx.hpp"

#include "utility.hpp"
#include <glm/gtx/string_cast.hpp>

std::string to_string(glm::vec3 const & vec) noexcept
{
    return glm::to_string(vec);
}

glm::vec3 rotate(glm::mat4 const & mat, glm::vec3 const & vec) noexcept
{
    return glm::vec3{mat * glm::vec4{vec, 1}};
}

std::ostream& operator<<(std::ostream& str, glm::vec3 const & vec) noexcept
{
    return str << glm::to_string(vec);
}

std::ostream& operator<<(std::ostream& str, glm::vec4 const & vec) noexcept
{
    return str << glm::to_string(vec);
}

glm::vec2 get_coordinates(unsigned width, unsigned index)
{
    unsigned const x {index % width};
    unsigned const y {(index - x) / width};
    return {x,y};
}

vertex from_position(glm::vec3 && val)
{
    return {std::move(val), {0,0}};
}

vertex from_position(float x, float y, float z)
{
    return {{x,y,z}, {0,0}};
}

float length_squared(glm::vec3 const & vec) noexcept
{
    return glm::dot(vec, vec);
}

float get_random_number(void)
{
    // static seed
    static std::mt19937 mt{1};
    static std::uniform_real_distribution<> dist{0, 1};
    return dist(mt);
}
