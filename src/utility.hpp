#pragma once

// This designates the width and bredth of a tile. All measurements are in meters.
const float tile_size {.5};
constexpr float PI=3.14159265;

enum struct field : char { left, right, mono };

float length_squared(glm::vec3 const &) noexcept;
glm::vec3 rotate(glm::mat4 const &, glm::vec3 const &) noexcept;
std::ostream& operator<<(std::ostream&, glm::vec3 const &) noexcept;
std::ostream& operator<<(std::ostream&, glm::vec4 const &) noexcept;
std::string to_string(glm::vec3 const &) noexcept;
glm::vec2 get_coordinates(unsigned width, unsigned index);

struct vertex
{
    glm::vec3 position;
    glm::vec2 tex_coord;
};

vertex from_position(glm::vec3 &&);
vertex from_position(float x, float y, float z);
float get_random_number(void);
